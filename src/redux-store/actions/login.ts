import { CALL_API } from "../middleware";
import { LOGIN, LoginPayload, LOGIN_SUCCESS } from "../types/login";

export const onLogin = (body: LoginPayload) => async (dispatch: any) => {
  try {
    return await dispatch({
      [CALL_API]: {
        url: `/admin/user/login`,
        method: "POST",
        types: [LOGIN_SUCCESS, LOGIN],
        body,
      },
    });
  } catch (error) {
    alert(`${error}`);
  }
};
