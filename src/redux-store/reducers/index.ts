import { combineReducers } from "redux"
import authReducer from "./login"

export interface  RootStateType { 
  authReducer : any,
  router : any
}

export const rootReducer = (history:any) => combineReducers({
    router: history,
    authReducer,
})

export type RootState = ReturnType< typeof rootReducer>