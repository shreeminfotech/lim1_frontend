import { ResponseBody } from "../middleware";
import { AuthState, AuthTypes, LOGIN_SUCCESS } from "../types/login";

const intiState: Partial<AuthState> = {
  token: 'sdsadsad',
};
function authReducer(state = intiState, action: AuthTypes & ResponseBody) {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { body } = action;
      const { data } = body;
      return {
        ...state,
        ...data,
      };
    }
    default:
      return state;
  }
}

export default authReducer;
