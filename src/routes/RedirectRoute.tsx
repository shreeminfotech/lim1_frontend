import { connect } from "react-redux";
import { Redirect, Route, RouteProps} from "react-router";
import { AuthType } from ".";
import { RootStateType } from "../redux-store/reducers";

function RedirectRoute({
  token,
  component: Component,
  ...rest
}: AuthType & RouteProps) {
  return (
    <Route
    {...rest}
      render={(props) =>
        !!token ? (
          <Redirect to="/home" from={props.location.pathname} />
        ) : (
          <Component {...props}/>
        )
      }
    />
  );
}

export default connect(({authReducer} : RootStateType) => {
  return { token : authReducer.token}
 }, {})(RedirectRoute)
 
