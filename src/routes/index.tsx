import React from "react";
import { Redirect, Route, Router, Switch } from "react-router-dom";
import Home from "../components/dashboard";
import Login from "../components/auth";
import { history } from "../store";
import ProtectedRoute from "./ProtectedRoute";
import RedirectRoute from "./RedirectRoute";

export interface AuthType {
  token: string | undefined;
  component: React.ComponentType<any>;
}

function AppRoutes() {
  return (
    <Router history={history}>
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => (
            <Redirect {...props} to="/home" from={props.location.pathname} />
          )}
        ></Route>
        <ProtectedRoute path="/home" component={Home} />
        <RedirectRoute path="/login" component={Login} />
      </Switch>
    </Router>
  );
}

export default AppRoutes;
