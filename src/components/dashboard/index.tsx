import { SIDENAV_ITEMS } from "../../utils/constants";
import SideBar from "../forms/SideBar";
import "./Dashboard.css";
import { AppBar, Toolbar } from "@material-ui/core";

function Home(props: any) {
  return (
    <div className="flex_column">
      <AppBar color="transparent" className="app_header" position="relative">
        <Toolbar>
          <img className = 'logo' alt='logo' src="/logo.png"></img>
        </Toolbar>
      </AppBar>
      <div className="flex_row home">
        <SideBar history = {props.history} list={SIDENAV_ITEMS} />
        <div className="route-body">
          {/* <body /> */}
        </div>
      </div>
    </div>
  );
}

export default Home;
