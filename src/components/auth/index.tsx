import { useState } from "react";
import { connect } from "react-redux";
import "../../App.scss";
import { Button } from "@material-ui/core";
import { onLogin } from "../../redux-store/actions/login";
import { LoginPayload } from "../../redux-store/types/login";
import { TextInput } from "../forms/textInput";

function Login(props: any) {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  async function _onLogin() {
    const reqBody: LoginPayload = { email, password };
    await props.onLogin(reqBody);
  }
  const onChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };
  const onChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  return (
    <div className="flex_column main_container">
      <div className="form_container">
        <div className="flex_row justify_s_between">
          <h1 className="signInHeadig">Log in</h1>
        </div>
        <div className="input-wrapper">
          <TextInput
            style={{ width: "100%" }}
            onChange={onChangeEmail}
            value={email}
            label={"Email Address"}
            type="text"
          ></TextInput>
        </div>

        <div className="input-wrapper">
          <TextInput
            style={{ width: "100%" }}
            onChange={onChangePassword}
            value={password}
            label={"Password"}
            type="password"
          ></TextInput>
          <p className="forgotPwd">Forgot Password?</p>
        </div>

        <div className="btn-container">
          <Button
            style={{
              width: "100%",
              color: "#ffffff",
              fontFamily: "Notosans-regular",
              fontSize: 16,
            }}
            onClick={_onLogin}
            className="login-btn"
          >
            Log In
          </Button>
        </div>
      </div>
    </div>
  );
}

export default connect(null, { onLogin })(Login);