import {TextField} from "@material-ui/core";

export function TextInput({
    label = "",
    value = "",
    errorMessage = "",
    isError = false,
    style = {},
    ...props
  }) {
    return (
      <TextField
        style = {{...style,fontFamily : 'Notosans-regular' }}
        value={value}
        label={label}
        id={label}
        defaultValue="Small"
        variant='outlined'
        helperText={errorMessage}
        error={isError}
        size = {'medium'}
        {...props}
      />
    );
  }

