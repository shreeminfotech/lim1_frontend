import { useState } from "react";
import { SideNavProps } from "../../utils/constants";
import "./SideBar.css";

function SideBar(props: any) {
  const [activeRoute, setActiveRoute] = useState<string>("Projects");
  return (
    <div className="left_nav">
      <div>
        {/* <p style={collapseIcon}>
            <img src="/images/collapse.svg" style={{ cursor: "pointer" }} />
          </p> */}
        {props.list.map(
          ({ activeImage, inactive, label, keyRoute }: SideNavProps) => (
            <div
              onClick={() => {
                props.history.push(`${keyRoute}`);
                setActiveRoute(label);
              }}
              key={label}
              className={activeRoute === label ? "nav_item active" : "nav_item"}
            >
              <img
                alt="img"
                src={activeRoute === label ? activeImage : inactive}
              />
              <p className="nav_label">{label}</p>
            </div>
          )
        )}
      </div>
    </div>
  );
}

export default SideBar;
