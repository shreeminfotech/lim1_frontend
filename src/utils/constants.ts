export interface SideNavProps {
  activeImage: string;
  inactive: string;
  label: string;
  keyRoute: string;
}

export const SIDENAV_ITEMS: SideNavProps[] = [
  {
    activeImage: "/icons/balance_active.svg",
    inactive: "/icons/balance_inactive.svg",
    label: "Projects",
    keyRoute: "home",
  },
  {
    activeImage: "/icons/admin_active.svg",
    inactive: "/icons/admin_inactive.svg",
    label: "Experiments",
    keyRoute: "home",
  }, 
  {
    activeImage: "/icons/admin_active.svg",
    inactive: "/icons/admin_inactive.svg",
    label: "Settings",
    keyRoute: "home",
  },
];
